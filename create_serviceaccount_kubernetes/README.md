#### 1. Create ServiceAccount on the clsuster

```sh
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
EOF
```

#### 2. Create ClusterRole and ClusterRoleBinding

```sh
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF
```

#### 3. Create and attach a token for your ServiceAccount

```sh
apiVersion: v1
kind: Secret
metadata:
  name: sa-gitlab-admin-token
  namespace: kube-system
  annotations:
    kubernetes.io/service-account.name: gitlab-admin
type: kubernetes.io/service-account-token
```
#### 4. command to retrieve your cluster's KUBE_URL

```sh 
kubectl cluster-info | grep 'Kubernetes control plane' | awk '/http/ {print $NF}'
```
#### 5. KUBE_CA_PEM_FILE
```sh
cat /etc/kubernetes/pki/ca.crt
```
##### ```/!\ WARNING : If you store this variable in gitlab, you must select "Type" => File ```

#### 6.  KUBE_TOKEN
```sh
SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
echo $TOKEN
```